Docker to build a custom Tryton Server image
============================================

# How to use this repo

## Configure environment variables

Complete .env file with backend user and password info.

## Complete repos.cfg modules file

Add sections to configparser file called "repos.cfg" with the desired Tryton modules.

```console
[DEFAULT]
branch = 5.4
repo = hg

[country]
url = http://hg.tryton.org/modules/country
path = core_modules
```

## Copy id_rsa*

Copy in root path your files id_rsa and id_rsa.pub for using in container

## Complete requirements.txt

Add your additional package dependencies

## Customize your docker-compose.yml

Copy the compose file sample and modify if proceed the services (trytond with uwsgi+nginx, cron, worker, ...)

```console
cp docker-compose.yml.sample docker-compose.yml
```

## Build image and run containers

```console
./start.sh
```
## Clone repositories

```console
docker exec -it tryton bash -c "invoke scm.clone; invoke scm.create-links"
```

## Update module repositories on container

```console
./6.0/tryton_bash.sh
invoke scm.pull -r country
trytond-admin -d <database> -u country -v
```